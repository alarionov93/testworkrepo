<?php 
require_once __DIR__.'/getDataFromDb.class.php';
if(isset($_POST['data'])) {
	$data = $_POST['data'];
	$dateRange = $data['dateRange'];
	$dType = $data['dType'];
	switch ($dateRange) {
		case 'last_6_month':
			$from = array(
				'0' => 'WHERE `date_of_sold` BETWEEN (SELECT CURDATE() - INTERVAL (DAYOFMONTH(CURDATE())) DAY - INTERVAL 1 MONTH) AND (SELECT CURDATE() - INTERVAL (DAYOFMONTH(CURDATE())) DAY)',
				'1' => 'WHERE `date_of_sold` BETWEEN (SELECT CURDATE() - INTERVAL (DAYOFMONTH(CURDATE())) DAY - INTERVAL 2 MONTH) AND (SELECT CURDATE() - INTERVAL (DAYOFMONTH(CURDATE())) DAY - INTERVAL 1 MONTH)',
				'2' => 'WHERE `date_of_sold` BETWEEN (SELECT CURDATE() - INTERVAL (DAYOFMONTH(CURDATE())) DAY - INTERVAL 3 MONTH) AND (SELECT CURDATE() - INTERVAL (DAYOFMONTH(CURDATE())) DAY - INTERVAL 2 MONTH)',
				'3' => 'WHERE `date_of_sold` BETWEEN (SELECT CURDATE() - INTERVAL (DAYOFMONTH(CURDATE())) DAY - INTERVAL 4 MONTH) AND (SELECT CURDATE() - INTERVAL (DAYOFMONTH(CURDATE())) DAY - INTERVAL 3 MONTH)',
				'4' => 'WHERE `date_of_sold` BETWEEN (SELECT CURDATE() - INTERVAL (DAYOFMONTH(CURDATE())) DAY - INTERVAL 5 MONTH) AND (SELECT CURDATE() - INTERVAL (DAYOFMONTH(CURDATE())) DAY - INTERVAL 4 MONTH)',
				'5' => 'WHERE `date_of_sold` BETWEEN (SELECT CURDATE() - INTERVAL (DAYOFMONTH(CURDATE())) DAY - INTERVAL 6 MONTH) AND (SELECT CURDATE() - INTERVAL (DAYOFMONTH(CURDATE())) DAY - INTERVAL 5 MONTH)',
				);
			$q_to = '';
			break;
		// case 'last_week':
		// 	$from = ' (SELECT (CURDATE() - WEEKDAY(CURDATE())+1) - INTERVAL 7 DAY) ';
		// 	$to = ' (SELECT (CURDATE() - WEEKDAY(CURDATE())+1)) ';
		// 	$q_from = ' WHERE `date_of_sold` >= '.$from.' ';
		// 	$q_to = ' AND `date_of_sold` < '.$to.' ';
		// 	break;
	}
	if (isset($data['Product'])) {
		$product = $data['Product'];
	}
	if (isset($data['Territory'])) {
		$territory = $data['Territory'];
	}
}
	$partOfQuery_1 = array();
	$partOfQuery_2 = array();
	if (isset($product)) {
		$prod = array();
		foreach ($product as $key => $value) {
			$end = '';
			if($key == 0) {
				$and = ' AND (';
			} else {
				$and = ' OR ';
			}
			if($key == (count($product)-1)) {
				$end = ')';
			}
			$add_prod = $and.'`product` = "'.$value.'"'.$end.' ';
			array_push($prod, $add_prod);
			array_push($partOfQuery_1, $add_prod);
		}
	}
	if (isset($territory)) {
		$terr = array();
		foreach ($territory as $key => $value) {
			$end = '';
			if($key == 0) {
				$and = ' AND (';
			} else {
				$and = ' OR ';
			}
			if($key == (count($territory)-1)) {
				$end = ')';
			}
			$add_terr = $and.'`t_name` = "'.$value.'"'.$end.' ';
			array_push($terr, $add_terr);
			array_push($partOfQuery_2, $add_terr);
		}
	}
	if(isset($product) | isset($territory)) {
		$partOfQuery_1 = implode(' ', $partOfQuery_1);
		$partOfQuery_2 = implode(' ', $partOfQuery_2);
		$partOfQuery = $partOfQuery_1.$partOfQuery_2;
	} else {
		$partOfQuery = '';
	}
	$driver = new GetDataFromDb();
	$mysqli = $driver->connect();
	$driver->getData($mysqli, $from, $q_to, $partOfQuery, $dType);
	$driver->disconnect($mysqli);
?>