currentDate = new Date();
myObj = {};
handle = {};
territories = [];
products = [];

$(document).ready(function () {
	last_6_month();
	$("#date_range").change(function () {
    var option_text = $("#date_range option:selected").text();
        switch (option_text) {
            case 'Last Week':
                last_week();
            break;
            case 'Last 6 Month by months':
                last_6_month();
            break;
        }
    });
    $("[name=displayType]").change(function () {
    	wayOfDisplay();
    });
	$("#insert").click(function () {
		$("#addData").slideToggle(200);
	});
	$("#change").change(function (evt) {
		var checkboxesIsSet = $("#change").children().children(":checkbox:checked");
		checkboxes = [];
		if (evt.target.name == 'territories') {
			handle.updateTerr = (function () {
				territories.push([evt.target.value, evt.target.id]);
				return territories;
			})();
		}
		if (evt.target.name == 'products') {
			handle.updateProd = (function () {
				products.push([evt.target.value, evt.target.id]);
				return products;
			})();
		}
		if (checkboxesIsSet.length !== 0 & evt.target.nextElementSibling.name == 'products') {
			handle.removeProd = (function () {
				var checked = [];
				var checkboxes = $("#change").find("div#product").children(":checkbox:checked");
				var i = checkboxes.length;
				for (var i = 0; i < checkboxes.length; i++) {
					checked.push([checkboxes[i].value, checkboxes[i].id]);
				};
				return checked;
			})();
		};
		if (checkboxesIsSet.length !== 0 & evt.target.nextElementSibling.name == 'territories') {
			handle.removeTerr = (function () {
				var checked = [];
				var checkboxes = $("#change").find("div#territories").children(":checkbox:checked");
				var i = checkboxes.length;
				for (var i = 0; i < checkboxes.length; i++) {
					checked.push([checkboxes[i].value, checkboxes[i].id]);
				};
				return checked;
			})();
		};
		console.log(handle);
	});
});
	function applyChanges() {
		$.ajax({
			type: 'POST',
			url: 'handle.php',
			dataType: 'html',
			data: {items: handle},
			success: function (resp) {
				console.log(resp);
				document.getElementById("response").innerHTML = resp;
			},
			error: function (resp) {
				console.log(resp.responseText); 
				document.getElementById("error").innerHTML = resp.responseText;
			}
		});
	}
	function wayOfDisplay() {
		if ($("[name=displayType]").find("input:radio:checked").val() == "table") {
			$('[name=territories]').children("input").attr("type", "checkbox");
			return 1;
		} else if ($("[name=displayType]").find("input:radio:checked").val() == "graphs") {
			$('[name=territories]').children("input").attr("type", "radio");
			return 0;
		}
	}
	function last_6_month() {
		myObj.dateRange = 'last_6_month';
        $("#Last_6_Month").val(myObj.dateRange);
    }

    function last_week() {
		myObj.dateRange = 'last_week';
        $("#Last_Week").val(myObj.dateRange);
    }
function getFormDrawData() {
	var data = {};
	data.dType = $("[name=displayType]").find("input:radio:checked").val();
	data.dateRange = (function () {
        var dateRange = myObj.dateRange;
        return dateRange;
    })();
	data.Product = (function () {
		var checked = [];
		var checkboxes = $('[name=products]').children("input:checked");
		var i = checkboxes.length;
		for (var i = 0; i < checkboxes.length; i++) {
			checked.push(checkboxes[i].name);
		};
		console.log(checked);
		return checked;
	})();
	data.Territory = (function () {
		var checked = [];
		var checkboxes = $('[name=territories]').children("input:checked");
		var i = checkboxes.length;
		for (var i = 0; i < checkboxes.length; i++) {
			checked.push(checkboxes[i].value);
		};
		console.log(checked);
		return checked;
	})();
	console.log(data);
	return data;
}
function sendDrawData() {
	data = getFormDrawData();
	$.ajax({
		type: 'POST',
		url: 'products.php',
		dataType: 'html',
		data: {data: data},
		success: function (resp) {
			console.log(resp);
			jsonResp = JSON.parse(resp);
			var dType = jsonResp.pop();
			var prodMeta = jsonResp.pop();
			console.log(jsonResp);
			console.log(prodMeta);
			if (dType == "table") {
				$("#placeholder").empty();
				displayTable(jsonResp);
			} else if (dType == "graphs") {
				$("table tbody").remove();
				dataDrawEnd = [];
				dataDraw = []; 
				var piece_0 = [];
				var piece_1 = [];
				var piece_2 = [];

				for(var j = 0; j < jsonResp.length; j++) {
				    jsonResp[j].date_of_sold = Date.parse(jsonResp[j].date_of_sold);
				}
				for (var n in jsonResp) {
					if(jsonResp[n].product == prodMeta[0].product) {
						piece_0.push([jsonResp[n].date_of_sold, jsonResp[n].saled]);
					} else if(jsonResp[n].product == prodMeta[1].product) {
						piece_1.push([jsonResp[n].date_of_sold, jsonResp[n].saled]);
					} else if(jsonResp[n].product == prodMeta[2].product) {
						piece_2.push([jsonResp[n].date_of_sold, jsonResp[n].saled]);
					}
				}
				if (piece_0.length !== 0) {
					dataDraw.push(piece_0);
				};
				if (piece_1.length !== 0) {
					dataDraw.push(piece_1);
				};
				if (piece_2.length !== 0) {
					dataDraw.push(piece_2);
				};
				console.log(dataDraw);
				dataDrawEnd=dataDraw;
				draw(dataDrawEnd);
			};
		},
		error: function (resp) {
			console.log(resp.responseText); 
			document.getElementById("error").innerHTML = resp.responseText;
		}
	});
}
function displayTable(json) {
	$("table tbody").remove();
	for (var n in jsonResp) {
		$("table").append('<tr><td>'+jsonResp[n].t_name+'</td><td>'+jsonResp[n].product+'</td><td>'+jsonResp[n].saled+'</td></tr>');
	}
}
function customData(dataArray) {
  var series = [];
      for (var i = 0; i < dataArray.length; i++) {
        series.push({
	        label: "Product "+(i+1),
	        data: dataDrawEnd[i],
	        lines: {
	            show: true,
	            fill: false,
	            lineWidth: 1,
	        },
        });
      };
      console.log(series);
      return series;
    };
function draw(dataDraw) {
    console.log(dataDrawEnd);
    data1 = customData(dataDrawEnd); 
    $.plot($("#placeholder"), data1, {
        xaxis: {
            min: (new Date(currentDate.getFullYear(), currentDate.getMonth()-4, currentDate.getDay()-5)).getTime(),
            max: (new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDay()+5)).getTime(),
            mode: "time",
            timeformat: "%d",
            tickSize: [1, "month"],
            monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            tickLength: 0, // hide gridlines
        },
        yaxis: {
          min: 0,
          max: 4000
        },
        grid: {
            hoverable: true,
            clickable: true,
            borderWidth: 1
        },
        legend: {
            labelBoxBorderColor: "none",
            position: "right"
        },
    });   
}