<!DOCTYPE html>
<?php require_once __DIR__.'/getDataFromDb.class.php';?>
<html>
<head>
	<title>Index</title>
	<meta charset="utf-8">
	<script type="text/javascript" src="jQuery.js"></script>
	<script type="text/javascript" src="flot/jquery.flot.js"></script>
	<script type="text/javascript" src="script.js"></script>
	<script type="text/javascript" src="flot/jquery.flot.time.js"></script>
	<style type="text/css">
	    body { 
	      font-family: Verdana, Arial, sans-serif;
	      font-size: 12px; 
	    }
	    h1 { width: 450px; 
	      margin: 0 auto; 
	      font-size: 12px; 
	      text-align: center; 
	    }
	    #placeholder { 
	      width: 450px; 
	      height: 200px; 
	      position: relative; 
	      margin: 0 auto; 
	    }
	    .legend table, .legend > div { 
	      height: 82px !important; 
	      opacity: 1 !important; 
	      right: -3px; top: 10px; 
	      width: 116px !important; 
	    }
	    .legend table { 
	      border: 1px solid #555;
	      padding: 5px;
	      background: cornsilk;
	    }
	    #flot-tooltip { 
	      font-size: 12px;
	      font-family: Verdana, Arial, sans-serif;
	      position: absolute;
	      display: none;
	      border: 2px solid;
	      padding: 2px;
	      background-color: #FFF;
	      opacity: 0.8;
	      -moz-border-radius: 5px;
	      -webkit-border-radius: 5px;
	      -khtml-border-radius: 5px;
	      border-radius: 5px;
	    }
	    #addData {
	    	position: absolute;
	    	display: none;
	    	background: #fff;
	    	border: 1px solid #000;
	    	padding: 10px;
	    	width: 990px;
	    }
  	</style>
</head>
<body>
	<input type="button" id="insert" value="Change Dimensions">
	<div id="addData">
		<b>Changing data into DB here</b>
		<?php
		printf('<form action="%s" method="post" id="change">', $_SERVER['PHP_SELF']);
			$driver = new GetDataFromDb();
			$mysqli = $driver->connect();
			echo '<div id="product"><p><label for="product">Choose product:</label></p>';
			$entity = $driver->getList($mysqli, 'prod_id', 'products', 'product');
			$driver->showList($entity, 'prod_id', 'product', 'products', 'text');
			echo '</div><div id="territories"><p><label for="territories">Choose territory:</label></p>';
			$entity = $driver->getList($mysqli, 't_id', 'territories', 't_name');
			$driver->showList($entity, 't_id', 't_name', 'territories', 'text');
		?>
		</div>
		<br>
		<p><input type="button" value="Apply changes" onclick="applyChanges()"></p>
		</form>
	</div>
	<div id="choose_1">Date range:</div>
		<select id="date_range" name="date_range">
			<option id="Last_6_Month" value="1">Last 6 Month by months</option>
		</select>
	<br><br>
	<?php
		echo '<p><label for="product">Choose product:</label></p>';
		$entity = $driver->getList($mysqli, 'prod_id', 'products', 'product');
		$driver->showList($entity, 'prod_id', 'product', 'products', 'checkboxes');
		echo '<p><label for="territory">Choose territory:</label></p>';
		$entity = $driver->getList($mysqli, 't_id', 'territories', 't_name');
		$driver->showList($entity, 't_id', 't_name', 'territories', 'radio');
		$driver->disconnect($mysqli);
	?>
	<form name="displayType">
		<p><label for="wayofdisplay">Choose way of display:</label></p>
		<p><input type="radio" name="dType" value="table">As table
		<input type="radio" name="dType" value="graphs">As graphs
		</p>
	</form>
	<br><input type="button" onclick="sendDrawData()" value="Send" />
	<div id="error"></div>
	<div id="response" style="position: absolute; background: #fff; border: 1px solid grey;"></div>
	<table name="data" border="0" style="width:60%">
		<thead>
			<tr>
				<td>
				Territory
				</td>
				<td>
				Product
				</td>
				<td>
				Sum Sold
				</td>
			</tr>
		</thead>
	</table>
	<div id="placeholder" style="width:600px;height:300px;"></div>
</body>
</html>