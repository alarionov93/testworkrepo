<?php
require_once __DIR__.'/getDataFromDb.class.php';
$driver = new GetDataFromDb();
$mysqli = $driver->connect();

if (isset($_POST['items'])) {
	$data = $_POST['items'];
	$driver->handle($mysqli, $data);
}
$driver->disconnect($mysqli);
?>