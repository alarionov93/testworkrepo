<?php
	/**
	* 
	*/
	class GetDataFromDb
	{
		private $conn;
		protected $data;

		function __construct() {
			$this->conn = array(
				'host' => 'localhost',
				'user' => 'root',
				'password' => '',
				'database' => 'prognoz_test'
			);
		}
		public function getList($mysqli, $id, $table, $column) {
			$query = $mysqli->real_escape_string('SELECT DISTINCT `'.$id.'`, `'.$column.'` FROM `'.$table.'`');
			if($result = $mysqli->query($query)) {
				$entity = array();
				while ($obj = $result->fetch_object()) {
					array_push($entity, $obj);
			   	} # Now we can use object with the results
			   	return $entity;
			}
		}
		public function showList($entity, $id, $column, $formName, $type) {
			if ($type == 'checkboxes') {
				echo '<p name="'.$formName.'">';
				foreach ($entity as $key => $obj) {
					printf ('<input type="checkbox" name="%s" id="%s"/>%s', $obj->$column, $obj->$id, $obj->$column);
				}
				echo '</p>';
			}
			if ($type == 'select') {
				echo '<p name="'.$formName.'">';
				echo '<select name="test">';
				foreach ($entity as $key => $obj) {
					printf ('<option name="%s" id="%s">%s</option>', $obj->$column, $obj->$id, $obj->$column);
				}
				echo '</select>';
				echo '</p>';
			}
			if ($type == 'text') {
				foreach ($entity as $key => $obj) {
					printf ('<input type="checkbox" value="%s" id="%s"/>Remove <input type="text" name="%s" value="%s" id="%s">', $obj->$column, $obj->$id, $formName, $obj->$column, $obj->$id, $obj->$column);
				}
			}
			if ($type == 'radio') {
				echo '<p name="'.$formName.'">';
				foreach ($entity as $key => $obj) {
					printf ('<input type="radio" name="territory" value="%s" id="%s"/>%s', $obj->$column, $obj->$id, $obj->$column);
				}
				echo '</p>';
			}
		}
		public function connect() {
			$conn = $this->conn;
			$mysqli = new mysqli($conn['host'], $conn['user'], $conn['password'], $conn['database']);
			if($mysqli->connect_error){
				die("Connect error: ".$mysqli->connect_error);
			}
			$mysqli->select_db($conn['database']);
			return $mysqli;
		}
		public function disconnect($mysqli) {
			$mysqli->close();
		}
		public function handle($mysqli, array $data) {
			try {
			if (!$mysqli->query('SET AUTOCOMMIT = 0')) {
				throw new Exception("Couldn't successfully run query! Error: ".$mysqli->error."\n");
			}
			$mysqli->begin_transaction();
			$aff = 0;
			if (isset($data['updateProd'])) {
				$productsUpdate = $data['updateProd'];
				# Update products
				foreach ($productsUpdate as $item) {
					$qu = 'UPDATE `products` SET `product` = ? WHERE `prod_id` = ?';
					if(FALSE === ($q=$mysqli->prepare($qu))) {
						throw new Exception("Statement on update products was not prepared!Error: ".$q->error."\n");
					} else {
						$q->bind_param("ss", $item[0], $item[1]); # Optional
						if(FALSE === $q->execute()) {
							throw new Exception("Query on update products was not executed!! Error: ".$q->error."\n");
						} else {
							$q->store_result();
							$aff+=$q->affected_rows;
							printf("Products were updated! Rows affected: %s", $aff);
							$q->free_result();
							$q->close();
						}
					}
				}
			}
			if (isset($data['updateTerr'])) {
				$territoriesUpdate = $data['updateTerr'];
				# Update territories
				foreach ($territoriesUpdate as $item) {
					$qu = 'UPDATE `territories` SET `t_name` = ? WHERE `t_id` = ?';
					if(FALSE === ($q=$mysqli->prepare($qu))) {
						throw new Exception("Statement on update territory was not prepared!Error: ".$q->error."\n");
					} else {
						$q->bind_param("ss", $item[0], $item[1]); # Optional
						if(FALSE === $q->execute()) {
							throw new Exception("Query on update territory was not executed!! Error: ".$q->error."\n");
						} else {
							$q->store_result();
							$aff+=$q->affected_rows;
							printf("Territories were updated! Rows affected: %s", $aff);
							$q->free_result();
							$q->close();
						}
					}
				}
			}
			if (isset($data['removeProd'])) {
				$productsRemove = $data['removeProd'];
				# Remove products
				foreach ($productsRemove as $item) {
					$qu = 'DELETE FROM `products` WHERE `product` = ?';
					$cascade = 'DELETE FROM `fuckts` WHERE `prod_id` = ?';
					if(FALSE === ($q=$mysqli->prepare($qu))) {
						throw new Exception("Statement on remove products was not prepared!Error: ".$q->error."\n");
					} else {
						$q->bind_param("s", $item[0]); # Optional
						if(FALSE === $q->execute()) {
							throw new Exception("Query on remove products was not executed!! Error: ".$q->error."\n");
						} else {
							$q->store_result();
							$aff+=$q->affected_rows;
							printf("Products were removed! Rows affected: %s", $aff);
							$q->free_result();
							$q->close();
						}
					}
					if(FALSE === ($q=$mysqli->prepare($cascade))) {
						throw new Exception("Statement on remove products from facts was not prepared!Error: ".$q->error."\n");
					} else {
						$q->bind_param("s", $item[1]); # Optional
						if(FALSE === $q->execute()) {
							throw new Exception("Query on remove products from facts was not executed!! Error: ".$q->error."\n");
						} else {
							$q->store_result();
							$aff+=$q->affected_rows;
							printf("Products were removed from facts! Rows affected: %s", $aff);
							$q->free_result();
							$q->close();
						}
					}
				}
			}
			if (isset($data['removeTerr'])) {
				$territoriesRemove = $data['removeTerr'];
				# Remove territories
				foreach ($territoriesRemove as $item) {
					$qu = 'DELETE FROM `territories` WHERE `t_name` = ?';
					$cascade = 'DELETE FROM `fuckts` WHERE `t_id` = ?';
					if(FALSE === ($q=$mysqli->prepare($qu))) {
						throw new Exception("Statement on remove territory was not prepared!Error: ".$q->error."\n");
					} else {
						$q->bind_param("s", $item[0]); # Optional
						if(FALSE === $q->execute()) {
							throw new Exception("Query on remove territory was not executed!! Error: ".$q->error."\n");
						} else {
							$q->store_result();
							$aff+=$q->affected_rows;
							printf("Territories were removed! Rows affected: %s \n", $aff);
							$q->free_result();
							$q->close();
						}
					}
					if(FALSE === ($q=$mysqli->prepare($cascade))) {
						throw new Exception("Statement on remove territory from facts was not prepared!Error: ".$q->error."\n");
					} else {
						$q->bind_param("s", $item[1]); # Optional
						if(FALSE === $q->execute()) {
							throw new Exception("Query on remove territory from facts was not executed!! Error: ".$q->error."\n");
						} else {
							$q->store_result();
							$aff+=$q->affected_rows;
							printf("Territories were removed from facts! Rows affected: %s \n", $aff);
							$q->free_result();
							$q->close();
						}
					}
				}
			}
			$mysqli->commit();
			printf("Success! Rows affected total: %s \n", $aff);
		} catch (Exception $e) {
			echo $e->getMessage();
			$mysqli->rollback();
		}
		}
		public function setMetaData($mysqli, $data, $queryType) {
			$qu = 'INSERT INTO `fuckts` (`t_id`, `prod_id`, `d_id`, `saled`) VALUES ("?", "?", "?", "?")';
			if($q = $mysqli->prepare($qu)) {
				$q->bind_param("dddd", $t_id[$i], $prod_id[$i], $d_id[$i], $saled[$i]); # Optional
				if($q->execute()) {
					$q->store_result();
				} else {
					printf("Query was not executed!! Error: %s", $q->error);
				}
				printf("Rows affected: ", $q->affected_rows);
				$q->free_result();
				$q->close();
			} else {
				printf("Query was not executed!Error: %s", $q->error);
			}
		}
		public function getData($mysqli, array $from, $q_to, $partOfQuery, $dType) {
			$entity = array();
			foreach ($from as $value) {
				$q_from = $value;
				$qu = 'SELECT `product`, MAX(`date_of_sold`) AS `date_of_sold`, `t_name`, SUM(`saled`) as `saled` FROM `fuckts` INNER JOIN `products` USING (`prod_id`) INNER JOIN `territories` USING (`t_id`) INNER JOIN `dates` USING (`d_id`) '.$value.$q_to.$partOfQuery.' GROUP BY `prod_id`, `t_id` ORDER BY `prod_id` ASC';
				if($q = $mysqli->prepare($qu)) {
					if($q->execute()) {
					$result = $q->get_result();
						while ($obj = $result->fetch_object()) {
						   // $obj->date_of_sold = strtotime($obj->date_of_sold." UTC") * 1000;
					       $obj->saled = (int) $obj->saled;
					       array_push($entity, $obj);
					   	} # Now we can use object with the results
					} else {
					printf("Query was not executed!! Error: %s", $q->error);
				}
					$q->close();
				} else {
					printf("Query was not executed!Error: %s", $q->error);
				}
			}
			$products = $this->getList($mysqli, 'prod_id', 'products', 'product'); # Metadata about products
			array_push($entity, $products);
			array_push($entity, $dType);
			$json = json_encode($entity); # prepare for send
			echo strip_tags($json);	# 
			return $json;
		}
	}
?>